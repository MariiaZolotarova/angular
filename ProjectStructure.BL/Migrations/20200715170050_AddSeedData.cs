﻿using System;
using System.Diagnostics.CodeAnalysis;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ProjectStructure.BL.Migrations
{
    [ExcludeFromCodeCoverage]
    public partial class AddSeedData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Teams",
                columns: new[] { "Id", "CreatedAt", "Name" },
                values: new object[] { 1, new DateTime(2020, 7, 15, 17, 0, 49, 814, DateTimeKind.Utc).AddTicks(3414), "Best team" });

            migrationBuilder.InsertData(
                table: "Teams",
                columns: new[] { "Id", "CreatedAt", "Name" },
                values: new object[] { 2, new DateTime(2020, 7, 15, 17, 0, 49, 814, DateTimeKind.Utc).AddTicks(3702), "Loser team" });

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Id", "Birthday", "Email", "FirstName", "LastName", "Phone", "RegisteredAt", "TeamId" },
                values: new object[] { 1, new DateTime(2020, 7, 15, 17, 0, 49, 813, DateTimeKind.Utc).AddTicks(8702), "email@gmail.com", "Jhon", "Labovski", "111", new DateTime(2020, 7, 15, 17, 0, 49, 813, DateTimeKind.Utc).AddTicks(9603), 1 });

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Id", "Birthday", "Email", "FirstName", "LastName", "Phone", "RegisteredAt", "TeamId" },
                values: new object[] { 2, new DateTime(2020, 7, 15, 17, 0, 49, 814, DateTimeKind.Utc).AddTicks(102), "email1@gmail.com", "Will", "Deapy", "222", new DateTime(2020, 7, 15, 17, 0, 49, 814, DateTimeKind.Utc).AddTicks(128), 2 });

            migrationBuilder.InsertData(
                table: "Projects",
                columns: new[] { "Id", "AuthorId", "CreatedAt", "DeadLine", "Description", "Name", "TeamId" },
                values: new object[] { 1, 1, new DateTime(2020, 7, 15, 17, 0, 49, 813, DateTimeKind.Utc).AddTicks(6224), new DateTime(2020, 7, 15, 17, 0, 49, 813, DateTimeKind.Utc).AddTicks(6547), "The best project ever", "Super Project", 1 });

            migrationBuilder.InsertData(
                table: "Projects",
                columns: new[] { "Id", "AuthorId", "CreatedAt", "DeadLine", "Description", "Name", "TeamId" },
                values: new object[] { 2, 2, new DateTime(2020, 7, 15, 17, 0, 49, 813, DateTimeKind.Utc).AddTicks(7794), new DateTime(2020, 7, 15, 17, 0, 49, 813, DateTimeKind.Utc).AddTicks(7801), "The worst project ever", "Bad Project", 2 });

            migrationBuilder.InsertData(
                table: "Tasks",
                columns: new[] { "Id", "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { 1, new DateTime(2020, 7, 15, 17, 0, 49, 814, DateTimeKind.Utc).AddTicks(1118), "The best task ever", new DateTime(2020, 7, 15, 17, 0, 49, 814, DateTimeKind.Utc).AddTicks(1411), "Super task", 1, 1, 0 });

            migrationBuilder.InsertData(
                table: "Tasks",
                columns: new[] { "Id", "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { 2, new DateTime(2020, 7, 15, 17, 0, 49, 814, DateTimeKind.Utc).AddTicks(2610), "The hardest task ever", new DateTime(2020, 7, 15, 17, 0, 49, 814, DateTimeKind.Utc).AddTicks(2617), "Hard task", 2, 2, 0 });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 2);
        }
    }
}
