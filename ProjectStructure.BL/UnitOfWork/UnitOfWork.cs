﻿using System.Threading.Tasks;
using ProjectStructure.BL.Context;
using ProjectStructure.BL.Context.Entity;
using ProjectStructure.BL.Repository;

namespace ProjectStructure.BL.UnitOfWork
{
    public class UnitOfWork : IUnitOfWork
    {
        protected readonly ProjectContext context;

        public UnitOfWork(ProjectContext context)
        {
            this.context = context;
        }

        public IRepository<TEntity> Set<TEntity>() where TEntity : Entity
        {
            return new Repository<TEntity>(context);
        }

        public async Task<int> SaveChangesAsync()
        {
            return await context.SaveChangesAsync();
        }
    }
}
