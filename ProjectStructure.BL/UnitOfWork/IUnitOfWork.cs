﻿using System.Threading.Tasks;
using ProjectStructure.BL.Context.Entity;
using ProjectStructure.BL.Repository;

namespace ProjectStructure.BL.UnitOfWork
{
    public interface IUnitOfWork
    {
        IRepository<TEntity> Set<TEntity>() where TEntity : Entity;
        Task<int> SaveChangesAsync();
    }
}
