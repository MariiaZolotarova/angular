﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using ProjectStructure.BL.Context.Entity;
using Task = System.Threading.Tasks.Task;

namespace ProjectStructure.BL.Repository
{
    public interface IRepository<TEntity> where TEntity: Entity
    {
        Task<IEnumerable<TEntity>> GetAsync(Expression<Func<TEntity, bool>> filter = null);
        Task CreateAsync(TEntity entity);
        void Update(TEntity entity);
        Task DeleteAsync(object id);
        void Delete(TEntity entity);
    }
}
