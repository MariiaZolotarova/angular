﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using ProjectStructure.BL.Context.Entity;
using ProjectStructure.Models;
using Xunit;
using Task = ProjectStructure.BL.Context.Entity.Task;

namespace ProjectStructure.IntegrationTests
{
    public class CollectionControllerIntegrationTest : IClassFixture<CustomWebApplicationFactory<Startup>>, IDisposable
    {
        private readonly HttpClient _client;

        public CollectionControllerIntegrationTest(CustomWebApplicationFactory<Startup> factory)
        {
            _client = factory.CreateClient();
        }

        public void Dispose()
        {
        }

        [Fact]
        public async System.Threading.Tasks.Task GetSomeUserTasks_WhenGetTasksInProject_GetCountOfTasksSomeUser()
        {
            // Arrange
            var projectDto = new ProjectDto
            {
                Name = "Abc",
                DeadLine = DateTime.UtcNow,
                Description = "Hello"
            };
            var taskDto = new TaskDto
            {
                Name = "DreamTask",
                State = StateEnum.Started,
                Description = "description"
            };
            var teamDto = new TeamDto
            {
                Name = "DreamTeam"
            };
            var userDto = new UserDto
            {
                LastName = "Flobby",
                Birthday = DateTime.UtcNow,
                Email = "flobby@mail",
                FirstName = "Robby"
            };

            // Act
            var team1 = await CreateDataAsync<TeamDto, Team>("api/teams", teamDto);
            userDto.TeamId = team1.Id;
            projectDto.TeamId = team1.Id;
            var user1 = await CreateDataAsync<UserDto, User>("api/users", userDto);
            projectDto.AuthorId = user1.Id;
            taskDto.PerformerId = user1.Id;
            var project1 = await CreateDataAsync<ProjectDto, Project>("api/projects", projectDto);
            taskDto.ProjectId = project1.Id;
            var task1 = await CreateDataAsync<TaskDto, Task>("api/tasks", taskDto);
            
            var responseTasks = await _client.GetAsync($"api/Collection/ShowCountProjectsWithTasks/{user1.Id}");
            var contentTasks = await responseTasks.Content.ReadAsStringAsync();
            var tasksResult = JsonConvert.DeserializeObject<Dictionary<string, int>>(contentTasks);

            await _client.DeleteAsync($"api/tasks/{task1.Id}");
            await _client.DeleteAsync($"api/projects/{project1.Id}");
            await _client.DeleteAsync($"api/users/{user1.Id}");
            await _client.DeleteAsync($"api/teams/{team1.Id}");

            //Assert
            Assert.Single(tasksResult);
            Assert.Equal(projectDto.Name, tasksResult.First().Key);
            Assert.Equal(1, tasksResult.First().Value);
        }
        private async Task<F> CreateDataAsync<T, F>(string url, T dto)
        {
            var response = await _client.PostAsync(url, new StringContent(JsonConvert.SerializeObject(dto), Encoding.UTF8, "application/json"));
            var content = await response.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<F>(content);
        }
    }
}
