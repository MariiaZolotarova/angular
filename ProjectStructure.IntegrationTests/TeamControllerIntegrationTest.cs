﻿using System;
using System.Net;
using System.Net.Http;
using System.Text;
using Newtonsoft.Json;
using ProjectStructure.BL.Context.Entity;
using ProjectStructure.Models;
using Xunit;
using Task = System.Threading.Tasks.Task;

namespace ProjectStructure.IntegrationTests
{
    public class TeamControllerIntegrationTest : IClassFixture<CustomWebApplicationFactory<Startup>>, IDisposable
    {
        private readonly HttpClient _client;

        public TeamControllerIntegrationTest(CustomWebApplicationFactory<Startup> factory)
        {
            _client = factory.CreateClient();
        }

        public void Dispose()
        {
        }

        [Fact]
        public async Task WhenCreateTeam_CheckIfOk()
        {
            // Arrange
            var teamDto = new TeamDto
            {
               Name = "DreamTeam"
            };
            var json = JsonConvert.SerializeObject(teamDto);

            // Act
            var response = await _client.PostAsync("api/teams", new StringContent(json, Encoding.UTF8, "application/json"));
            var content = await response.Content.ReadAsStringAsync();
            var team = JsonConvert.DeserializeObject<Team>(content);

            await _client.DeleteAsync($"api/teams/{team.Id}");

            // Assert
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
            Assert.Equal(teamDto.Name, team.Name);
        }

        [Fact]
        public async Task WhenCreateTeam_CheckBadRequest()
        {
            // Arrange
            var teamDto = new TeamDto
            {
                Name = null
            };
            var json = JsonConvert.SerializeObject(teamDto);

            // Act
            var response = await _client.PostAsync("api/teams", new StringContent(json, Encoding.UTF8, "application/json"));

            // Assert
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }
    }
}
