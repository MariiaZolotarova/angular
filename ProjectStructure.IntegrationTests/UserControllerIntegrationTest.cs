﻿using System;
using System.Net;
using System.Net.Http;
using System.Text;
using Newtonsoft.Json;
using ProjectStructure.BL.Context.Entity;
using ProjectStructure.Models;
using Xunit;
using Task = System.Threading.Tasks.Task;

namespace ProjectStructure.IntegrationTests
{
    public class UserControllerIntegrationTest : IClassFixture<CustomWebApplicationFactory<Startup>>, IDisposable
    {
        private readonly HttpClient _client;

        public UserControllerIntegrationTest(CustomWebApplicationFactory<Startup> factory)
        {
            _client = factory.CreateClient();
        }

        public void Dispose()
        {
        }
        [Fact]
        public async Task WhenCreateUser_CheckIfOk()
        {
            // Arrange
            var userDto = new UserDto
            {
                TeamId = 1,
                LastName = "Flobby",
                Birthday = DateTime.UtcNow,
                Email = "flobby@mail",
                FirstName = "Robby"
            };
            var json = JsonConvert.SerializeObject(userDto);

            // Act
            var response = await _client.PostAsync("api/users", new StringContent(json, Encoding.UTF8, "application/json"));
            var content = await response.Content.ReadAsStringAsync();
            var user = JsonConvert.DeserializeObject<User>(content);

            await _client.DeleteAsync($"api/users/{user.Id}");

            // Assert
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
            Assert.Equal(userDto.FirstName, user.FirstName);
        }

        [Theory]
        [InlineData(null, "abc")]
        [InlineData("abc", null)]
        public async Task WhenCreateUser_CheckBadRequest(string firstName, string lastName)
        {
            // Arrange
            var userDto = new UserDto
            {
                TeamId = 1,
                LastName = lastName,
                Birthday = DateTime.UtcNow,
                Email = "flobby@mail",
                FirstName = firstName
            };
            var json = JsonConvert.SerializeObject(userDto);

            // Act
            var response = await _client.PostAsync("api/users", new StringContent(json, Encoding.UTF8, "application/json"));

            // Assert
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [Fact]
        public async Task WhenCreateUser_CheckException()
        {
            // Arrange
            var userDto = new UserDto
            {
                TeamId = -1,
                LastName = "Flobby",
                Birthday = DateTime.UtcNow,
                Email = "flobby@mail",
                FirstName = "Robby"
            };
            var json = JsonConvert.SerializeObject(userDto);

            // Act
            var response = await _client.PostAsync("api/users", new StringContent(json, Encoding.UTF8, "application/json"));

            // Assert
            Assert.Equal(HttpStatusCode.InternalServerError, response.StatusCode);
        }
    }
}
