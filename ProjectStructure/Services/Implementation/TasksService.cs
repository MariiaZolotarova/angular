﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using ProjectStructure.BL.Context.Entity;
using ProjectStructure.BL.UnitOfWork;
using ProjectStructure.Models;
using ProjectStructure.Services.Abstraction;
using Task = ProjectStructure.BL.Context.Entity.Task;

namespace ProjectStructure.Services.Implementation
{
    public class TasksService : ITasksService
    {
        private readonly IUnitOfWork unitOfWork;
        private readonly IMapper mapper;
        public TasksService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            this.unitOfWork = unitOfWork;
            this.mapper = mapper;
        }

        public async System.Threading.Tasks.Task<List<Task>> GetTasksAsync()
        {
            var taskRepository = unitOfWork.Set<Task>();
            return (await taskRepository.GetAsync()).ToList();
        }

        public async System.Threading.Tasks.Task<List<Task>> GetNoFinishedTasksAsync(int id)
        {
            var taskRepository = unitOfWork.Set<Task>();
            return (await taskRepository.GetAsync(x => x.PerformerId == id && x.State != StateEnum.Finished)).ToList();
        }

        public async System.Threading.Tasks.Task<Task> GetTaskIdAsync(int id)
        {
            var taskRepository = unitOfWork.Set<Task>();
            return (await taskRepository.GetAsync(x => x.Id == id)).FirstOrDefault();
        }
        public async System.Threading.Tasks.Task<Task> CreateTaskAsync(TaskDto taskDto)
        {
            if (taskDto == null || string.IsNullOrWhiteSpace(taskDto.Name))
            {
                throw new ArgumentNullException();
            }

            var taskRepository = unitOfWork.Set<Task>();
            var task = mapper.Map<Task>(taskDto);
            task.CreatedAt = DateTime.UtcNow;

            if (task.State == StateEnum.Finished)
            {
                task.FinishedAt = DateTime.UtcNow;
            }
            else
            {
                task.FinishedAt = null;
            }

            await taskRepository.CreateAsync(task);
            await unitOfWork.SaveChangesAsync();
            return task;
        }

        public async System.Threading.Tasks.Task DeleteTaskAsync(int id)
        {
            var taskRepository = unitOfWork.Set<Task>();

            await taskRepository.DeleteAsync(id);
            await unitOfWork.SaveChangesAsync();
        }

        public async System.Threading.Tasks.Task UpdateTaskAsync(int id, TaskDto taskDto)
        {
            var taskRepository = unitOfWork.Set<Task>();
            var task = mapper.Map<Task>(taskDto);
            task.Id = id;
            task.CreatedAt = DateTime.UtcNow;

            if (task.State == StateEnum.Finished)
            {
                task.FinishedAt = DateTime.UtcNow;
            }
            else
            {
                task.FinishedAt = null;
            }

            taskRepository.Update(task);
            await unitOfWork.SaveChangesAsync();
        }
    }
}
