﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using ProjectStructure.BL.Context.Entity;
using ProjectStructure.BL.UnitOfWork;
using ProjectStructure.Models;
using ProjectStructure.Services.Abstraction;
using Task = System.Threading.Tasks.Task;

namespace ProjectStructure.Services.Implementation
{
    public class UsersService : IUsersService
    {
        private readonly IUnitOfWork unitOfWork;
        private readonly IMapper mapper;

        public UsersService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            this.unitOfWork = unitOfWork;
            this.mapper = mapper;
        }

        public async Task<List<User>> GetUsersAsync()
        {
            var userRepository = unitOfWork.Set<User>();
            return (await userRepository.GetAsync()).ToList();
        }
       
        public async Task<User> GetUsersId(int id)
        {
            var userRepository = unitOfWork.Set<User>();
            return (await userRepository.GetAsync(x => x.Id == id)).FirstOrDefault();
        }

        public async Task<User> CreateUserAsync(UserDto userDto)
        {
            if (userDto == null || string.IsNullOrWhiteSpace(userDto.FirstName) ||
                string.IsNullOrWhiteSpace(userDto.LastName))
            {
                throw new ArgumentNullException();
            }

            var userRepository = unitOfWork.Set<User>();
            var user = mapper.Map<User>(userDto);
            user.RegisteredAt = DateTime.UtcNow;

            await userRepository.CreateAsync(user);
            await unitOfWork.SaveChangesAsync();
            return user;
        }

        public async Task DeleteUserAsync(int id)
        {
            var userRepository = unitOfWork.Set<User>();

            await userRepository.DeleteAsync(id);
            await unitOfWork.SaveChangesAsync();
        }
        public async Task UpdateUserAsync(int id, UserDto userDto)
        {
            var userRepository = unitOfWork.Set<User>();
            var user = mapper.Map<User>(userDto);
            user.Id = id;
            user.RegisteredAt = DateTime.UtcNow;

            userRepository.Update(user);
            await unitOfWork.SaveChangesAsync();
        }
    }
}
