﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using ProjectStructure.BL.Context.Entity;
using ProjectStructure.BL.UnitOfWork;
using ProjectStructure.Models;
using ProjectStructure.Services.Abstraction;
using Task = System.Threading.Tasks.Task;

namespace ProjectStructure.Services.Implementation
{
    public class ProjectsService : IProjectsService
    {
        private readonly IUnitOfWork unitOfWork;
        private readonly IMapper mapper;
        public ProjectsService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            this.unitOfWork = unitOfWork;
            this.mapper = mapper;
        }

        public async Task<List<Project>> GetProjectsAsync()
        {
            var projectRepository = unitOfWork.Set<Project>();
            return (await projectRepository.GetAsync()).ToList();
        }

        public async Task<Project> GetProjectIdAsync(int id)
        {
            var projectRepository = unitOfWork.Set<Project>();
            return (await projectRepository.GetAsync(x => x.Id == id)).FirstOrDefault();
        }
        public async Task<Project> CreateProjectAsync(ProjectDto projectDto)
        {
            if (projectDto == null || String.IsNullOrWhiteSpace(projectDto.Name))
            {
                throw new ArgumentNullException();
            }

            var projectRepository = unitOfWork.Set<Project>();
            var project = mapper.Map<Project>(projectDto);
            project.CreatedAt = DateTime.UtcNow;

            await projectRepository.CreateAsync(project);
            await unitOfWork.SaveChangesAsync();

            return project;
        }
        public async Task DeleteProjectAsync(int id)
        {
            var projectRepository = unitOfWork.Set<Project>();

            await projectRepository.DeleteAsync(id);
            await unitOfWork.SaveChangesAsync();
        }
        public async Task UpdateProjectAsync(int id, ProjectDto projectDto)
        {
            var projectRepository = unitOfWork.Set<Project>();
            var project = mapper.Map<Project>(projectDto);
            project.Id = id;
            project.CreatedAt = DateTime.UtcNow;

            projectRepository.Update(project);
            await unitOfWork.SaveChangesAsync();
        }
    }
}
