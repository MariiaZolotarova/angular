﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using ProjectStructure.BL.Context.Entity;
using ProjectStructure.BL.UnitOfWork;
using ProjectStructure.Models;
using ProjectStructure.Services.Abstraction;
using Task = System.Threading.Tasks.Task;

namespace ProjectStructure.Services.Implementation
{
    public class TeamsService : ITeamsService
    {
        private readonly IMapper mapper;
        private readonly IUnitOfWork unitOfWork;

        public TeamsService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            this.unitOfWork = unitOfWork;
            this.mapper = mapper;
        }

        public async Task<List<Team>> GetTeamsAsync()
        {
            var teamRepository = unitOfWork.Set<Team>();
            return (await teamRepository.GetAsync()).ToList();
        }

        public async Task<Team> GetTeamIdAsync(int id)
        {
            var teamRepository = unitOfWork.Set<Team>();
            return (await teamRepository.GetAsync(x => x.Id == id)).FirstOrDefault();
        }

        public async Task<Team> CreateTeamAsync(TeamDto teamDto)
        {
            if (teamDto == null || string.IsNullOrWhiteSpace(teamDto.Name))
            {
                throw new ArgumentNullException();
            }

            var teamRepository = unitOfWork.Set<Team>();
            var team = mapper.Map<Team>(teamDto);
            team.CreatedAt = DateTime.UtcNow;

            await teamRepository.CreateAsync(team);
            await unitOfWork.SaveChangesAsync();
            return team;
        }
        public async Task DeleteTeamAsync(int id)
        {
            var teamRepository = unitOfWork.Set<Team>();

            await teamRepository.DeleteAsync(id);
            await unitOfWork.SaveChangesAsync();
        }

        public async Task UpdateTeamAsync(int id, TeamDto teamDto)
        {
            var teamRepository = unitOfWork.Set<Team>();
            var team = mapper.Map<Team>(teamDto);
            team.Id = id;
            team.CreatedAt = DateTime.UtcNow;

            teamRepository.Update(team);
            await unitOfWork.SaveChangesAsync();
        }

    }
}
