﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ProjectStructure.BL.Context.Entity;
using ProjectStructure.Models;
using Task = System.Threading.Tasks.Task;

namespace ProjectStructure.Services.Abstraction
{
    public interface ITeamsService
    {
        Task<List<Team>> GetTeamsAsync();
        Task<Team> GetTeamIdAsync(int id);
        Task<Team> CreateTeamAsync(TeamDto teamDto);
        Task DeleteTeamAsync(int id);
        Task UpdateTeamAsync(int id, TeamDto teamDto);
    }
}
