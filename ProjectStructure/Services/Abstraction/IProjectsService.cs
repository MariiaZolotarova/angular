﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ProjectStructure.BL.Context.Entity;
using ProjectStructure.Models;
using Task = System.Threading.Tasks.Task;

namespace ProjectStructure.Services.Abstraction
{
    public interface IProjectsService
    {
        Task<List<Project>> GetProjectsAsync();
        Task<Project> GetProjectIdAsync(int id);
        Task<Project> CreateProjectAsync(ProjectDto projectDto);
        Task DeleteProjectAsync(int id);
        Task UpdateProjectAsync(int id, ProjectDto projectDto);
    }
}
