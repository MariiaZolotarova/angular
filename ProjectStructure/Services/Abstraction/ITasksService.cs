﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ProjectStructure.Models;
using Task = ProjectStructure.BL.Context.Entity.Task;

namespace ProjectStructure.Services.Abstraction
{
    public interface ITasksService
    {
        Task<List<Task>> GetTasksAsync();
        System.Threading.Tasks.Task<Task> GetTaskIdAsync(int id); //??
        Task<List<Task>> GetNoFinishedTasksAsync(int id);
        System.Threading.Tasks.Task<Task> CreateTaskAsync(TaskDto taskDto); //??
        System.Threading.Tasks.Task DeleteTaskAsync(int id);
        System.Threading.Tasks.Task UpdateTaskAsync(int id, TaskDto taskDto);
    }
}
