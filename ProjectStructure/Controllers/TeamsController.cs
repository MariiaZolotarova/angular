﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ProjectStructure.BL.Context.Entity;
using ProjectStructure.Models;
using ProjectStructure.Services.Abstraction;

namespace ProjectStructure.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TeamsController : ControllerBase
    {
        private readonly ITeamsService teamsService;

        public TeamsController(ITeamsService teamsService)
        {
            this.teamsService = teamsService;
        }
        [HttpGet]
        public async Task<List<Team>> GetTeamsAsync()
        {
            return await teamsService.GetTeamsAsync();
        }
        [HttpGet("{id}")]
        public async Task<Team> GetTeamsIdAsync([FromRoute] int id)
        {
            return await teamsService.GetTeamIdAsync(id);
        }

        [HttpPost]
        public async Task<ActionResult<Team>> CreateTeam([FromBody] TeamDto team)
        {
            try
            {
                return Ok(await teamsService.CreateTeamAsync(team));
            }
            catch (ArgumentNullException exception)
            {
                return BadRequest(exception);
            }
            catch (Exception exception)
            {
                return StatusCode(500, exception);
            }

        }

        [HttpDelete("{id}")]
        public async System.Threading.Tasks.Task DeleteTeamAsync([FromRoute] int id)
        {
            await teamsService.DeleteTeamAsync(id);
        }

        [HttpPut("{id}")]
        public async System.Threading.Tasks.Task UpdateTeamAsync([FromRoute] int id, [FromBody] TeamDto team)
        {
            await teamsService.UpdateTeamAsync(id, team);
        }
    }
}
