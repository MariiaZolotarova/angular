﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ProjectStructure.BL.Context.Entity;
using ProjectStructure.Models;
using ProjectStructure.Services.Abstraction;
using Task = System.Threading.Tasks.Task;

namespace ProjectStructure.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly IUsersService usersService;

        public UsersController(IUsersService usersService)
        {
            this.usersService = usersService;
        }

        [HttpGet]
        public async Task<List<User>> GetUsers()
        {
            return await usersService.GetUsersAsync();
        }

        [HttpGet("{id}")]
        public async Task<User> GetUsersId([FromRoute] int id)
        {
            return await usersService.GetUsersId(id);
        }

        [HttpPost]
        public async Task<ActionResult<User>> CreateUser([FromBody] UserDto user)
        {
            try
            {
                return  Ok(await usersService.CreateUserAsync(user));
            }
            catch (ArgumentNullException exception)
            {
                return BadRequest(exception);
            }
            catch (Exception exception)
            {
                return StatusCode(500, exception);
            }
        }

        [HttpDelete("{id}")]
        public async Task DeleteUser([FromRoute] int id)
        {
            await usersService.DeleteUserAsync(id);
        }

        [HttpPut("{id}")]
        public async Task UpdateUserAsync([FromRoute] int id, [FromBody] UserDto user)
        {
           await usersService.UpdateUserAsync(id, user);
        }
    }
}
