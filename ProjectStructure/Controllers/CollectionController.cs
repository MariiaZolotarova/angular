﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using ProjectStructure.BL.Context.Entity;
using ProjectStructure.Models;
using ProjectStructure.Services.Abstraction;

namespace ProjectStructure.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CollectionController : ControllerBase
    {
        private readonly ICollectionService collectionService;
        public CollectionController(ICollectionService collectionService)
        {
            this.collectionService = collectionService;
        }

        [HttpGet("ShowCountProjectsWithTasks/{userId}")]
        public async System.Threading.Tasks.Task<Dictionary<string, int>> GetProjectsAsync([FromRoute] int userId)
        {
            return await collectionService.ShowCountProjectsWithTasksAsync(userId);
        }

        [HttpGet("ShowDesignedTasksForSpecialUser/{userId}")]
        public async System.Threading.Tasks.Task<List<Task>> ShowDesignedTasksForSpecialUserAsync([FromRoute] int userId)
        {
            return await collectionService.ShowDesignedTasksForSpecialUserAsync(userId);
        }

        [HttpGet("ShowPerformedTasksInCurrentYear/{userId}")]
        public async System.Threading.Tasks.Task<List<NameId>> ShowPerformedTasksInCurrentYearAsync([FromRoute] int userId)
        {
            return await collectionService.ShowPerformedTasksInCurrentYearAsync(userId);
        }

        [HttpGet("ShowListOfTeamsOlderThan")]
        public async System.Threading.Tasks.Task<List<TeamUsers>> ShowListOfTeamsOlderThanAsync()
        {
            return await collectionService.ShowListOfTeamsOlderThanAsync();
        }

        [HttpGet("ShowListOfUsersWithSortedTasks")]
        public async System.Threading.Tasks.Task<List<UserTasks>> ShowListOfUsersWithSortedTasksAsync()
        {
            return await collectionService.ShowListOfUsersWithSortedTasksAsync();
        }

        [HttpGet("StructOfUser/{userId}")]
        public async System.Threading.Tasks.Task<ProjectTasks> StructOfUserAsync([FromRoute] int userId)
        {
            return await collectionService.StructOfUserAsync(userId);
        }

        [HttpGet("StructOfProjects")]
        public async System.Threading.Tasks.Task<List<ProjectInfo>> StructOfProjectsAsync()
        {
            return await collectionService.StructOfProjectsAsync();
        }
    }
}
