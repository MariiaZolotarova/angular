﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ProjectStructure.BL.Context.Entity;

namespace ProjectStructure.Models
{
    public class TeamUsers
    {
        public int? TeamId { get; set; }
        public string Name { get; set; }
        public List<User> Users { get; set; }
    }
}
