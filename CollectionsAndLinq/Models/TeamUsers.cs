﻿using System.Collections.Generic;

namespace CollectionsAndLinq.Models
{
    class TeamUsers
    {
        public int? TeamId { get; set; }
        public string Name { get; set; }
        public List<UsersModel> Users { get; set; }
    }
}
