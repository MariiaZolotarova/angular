﻿using System;
using System.Collections.Generic;

namespace CollectionsAndLinq.Models
{
    public class ProjectModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime DeadLine { get; set; }
        public int AuthorId { get; set; }
        public UsersModel Author { get; set; }
        public int TeamId { get; set; }
        public TeamsModel Team { get; set; }
        public List<TaskModel> Tasks { get; set; } 
    }
}
