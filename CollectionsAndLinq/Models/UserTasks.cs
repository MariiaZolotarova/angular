﻿using System.Collections.Generic;

namespace CollectionsAndLinq.Models
{
    public class UserTasks
    {
        public List<TaskModel> Tasks { get; set; }
        public UsersModel User { get; set; }
    }
}
