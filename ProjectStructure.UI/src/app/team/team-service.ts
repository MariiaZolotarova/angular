import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { Observable } from 'rxjs';
import { TeamDto } from './team-create/team-create.component';

@Injectable()
export class TeamHttpService{

    constructor(private http: HttpClient){ }

    getTeam(): Observable<object>{
        return this.http.get('https://localhost:44379/api/Teams');
    }

    deleteTeam(id): Observable<object>{
        return this.http.delete(`https://localhost:44379/api/Teams/${id}`);
    }

    postTeam(team: TeamDto): Observable<object>{
        return this.http.post('https://localhost:44379/api/Teams', team);
    }

    getTeamById(id): Observable<object>{
        return this.http.get(`https://localhost:44379/api/Teams/${id}`);
    }

    updateTeam(id, team: TeamDto): Observable<object>{
        return this.http.put(`https://localhost:44379/api/Teams/${id}`, team);
    }
}
