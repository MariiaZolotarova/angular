import { TeamDto } from '../team-create/team-create.component';
import { Component, OnInit } from '@angular/core';
import { TeamHttpService } from '../team-service';

export interface ITeam {
  id: number;
  name: string;
  createdAt: string;
}

@Component({
  selector: 'app-team-table',
  templateUrl: './team-table.component.html',
  styleUrls: ['./team-table.component.css'],
  providers: [TeamHttpService]
})

export class TeamTableComponent implements OnInit {
  displayedColumns: string[] = ['id', 'name', 'createdAt', 'editTeam'];

  teams: ITeam[];
  constructor(private httpService: TeamHttpService) { }
  ngOnInit(): void {
    this.httpService.getTeam().subscribe((data: ITeam[]) => this.teams = data);
  }
  removeTeam(id: number): any {
    this.httpService.deleteTeam(id).subscribe((data: ITeam[]) => this.teams = this.teams.filter(x => x.id !== id));
  }
}
