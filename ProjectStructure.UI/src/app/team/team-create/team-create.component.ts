import { Component, OnInit, Input } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { TeamHttpService } from 'src/app/team/team-service';
import { ActivatedRoute } from '@angular/router';

export interface TeamDto {
  name: string;
}

@Component({
  selector: 'app-team-create',
  templateUrl: './team-create.component.html',
  styleUrls: ['./team-create.component.css'],
  providers: [TeamHttpService]
})

export class TeamCreateComponent implements OnInit {

  @Input() team: TeamDto = {} as TeamDto;
  teamForm: FormGroup;
  teamId: string;
  checkoutForm;

  constructor(private httpService: TeamHttpService, private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.teamId = this.route.snapshot.paramMap.get('id');

    if (this.teamId === null) {
      this.initTeamForm();
    }
    else {
      this.httpService.getTeamById(this.teamId).subscribe((data: TeamDto) => {
        this.team = data;
        this.initTeamForm();
      });
    }
  }

  initTeamForm(): void {
    this.teamForm = new FormGroup({
      name: new FormControl(this.team.name, [
        Validators.required
      ])
    });
  }

  saveTeam(): void {
    const newTeam = this.teamForm.value;

    if (this.teamId === null) {
      this.httpService.postTeam(newTeam).subscribe(() => { });
      this.teamForm.reset();
    }
    else {
      this.httpService.updateTeam(this.teamId, newTeam).subscribe(() => { });
    }
  }
}
