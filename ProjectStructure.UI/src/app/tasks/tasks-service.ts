import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { Observable } from 'rxjs';
import { TaskDto } from './tasks-create/tasks-create.component';

@Injectable()
export class TasksHttpService{

    constructor(private http: HttpClient){ }

    getTasks(): Observable<object>{
        return this.http.get('https://localhost:44379/api/Tasks');
    }

    deleteTasks(id): Observable<object>{
        return this.http.delete(`https://localhost:44379/api/Tasks/${id}`);
    }

    postTask(task: TaskDto): Observable<object>{
        return this.http.post('https://localhost:44379/api/Tasks', task);
    }

    getTaskById(id): Observable<object>{
        return this.http.get(`https://localhost:44379/api/Tasks/${id}`);
    }

    updateTask(id, task: TaskDto): Observable<object>{
        return this.http.put(`https://localhost:44379/api/Tasks/${id}`, task);
    }
}
