import { TasksHttpService } from '../tasks-service';
import { Component, OnInit , Input} from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';

export interface TaskDto {
  name: string;
  description: string;
  state: string;
  projectId: string;
  performerId: number;
}

@Component({
  selector: 'app-tasks-create',
  templateUrl: './tasks-create.component.html',
  styleUrls: ['./tasks-create.component.css'],
  providers: [TasksHttpService]
})

export class TasksCreateComponent implements OnInit {

  @Input() task: TaskDto = {} as TaskDto;
  tasksForm: FormGroup;

  checkoutForm;
  taskId: string;

  constructor(private httpService: TasksHttpService, private route: ActivatedRoute){}

  ngOnInit(): void {
    this.taskId = this.route.snapshot.paramMap.get('id');

    if (this.taskId === null) {
      this.initUserForm();
    }
    else {
      this.httpService.getTaskById(this.taskId).subscribe((data: TaskDto) => {
        this.task = data;
        this.initUserForm();
      });
    }
  }

  initUserForm(): void {
    this.tasksForm = new FormGroup({
      name: new FormControl(this.task.name, [
        Validators.required
      ]),
      description: new FormControl(this.task.description, [
        Validators.required
      ]),
      state: new FormControl(this.task.state, [
        Validators.required
      ]),
      projectId: new FormControl(this.task.projectId, [
        Validators.required
      ]),
      performerId: new FormControl(this.task.performerId, [
        Validators.required
      ])
    });
  }

  saveTasks(): void {
    const newTeam = this.tasksForm.value;

    if (this.taskId === null) {
      this.httpService.postTask(newTeam).subscribe(() => { });
      this.tasksForm.reset();
    }
    else {
      this.httpService.updateTask(this.taskId, newTeam).subscribe(() => { });
    }
  }
}
