import { Component, OnInit } from '@angular/core';
import { TasksHttpService} from '../tasks-service';
import { TaskDto } from '../tasks-create/tasks-create.component';

export interface ITasks {
  id: number;
  name: string;
  description: string;
  createdAt: string;
  finishedAt: string;
  state: string;
  projectId: number;
  performerId: number;
}

@Component({
  selector: 'app-tasks-table',
  templateUrl: './tasks-table.component.html',
  styleUrls: ['./tasks-table.component.css'],
  providers: [TasksHttpService]
})

export class TasksTableComponent implements OnInit {
  displayedColumns: string[] = ['id', 'name', 'description', 'createdAt', 'finishedAt', 'state', 'projectId', 'performerId', 'editTasks'];
  // dataSource = TASKS_DATA;

  tasks: ITasks[];
  constructor(private httpService: TasksHttpService){}
  ngOnInit(): void{
      this.httpService.getTasks().subscribe((data: ITasks[]) => this.tasks = data);
  }

  removeTasks(id: number): any {
    this.httpService.deleteTasks(id).subscribe((data: ITasks[]) => this.tasks = this.tasks.filter(x => x.id !== id));
  }
}
