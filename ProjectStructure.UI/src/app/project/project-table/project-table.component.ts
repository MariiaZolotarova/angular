import { Component, OnInit } from '@angular/core';
import { ProjectHttpService} from '../project-service';
import { ProjectDto } from '../project-create/project-create.component';

export interface IProject {
  id: number;
  name: string;
  description: string;
  createdAt: string;
  deadline: string;
  authorId: number;
  teamId: number;
}

@Component({
  selector: 'app-project-table',
  templateUrl: './project-table.component.html',
  styleUrls: ['./project-table.component.css'],
  providers: [ProjectHttpService]
})

export class ProjectTableComponent implements OnInit {
  displayedColumns: string[] = ['id', 'name', 'description', 'createdAt', 'authorId', 'teamId', 'editProject'];

  projects: IProject[];
  constructor(private httpService: ProjectHttpService){}
  ngOnInit(): void{
      this.httpService.getProject().subscribe((data: IProject[]) => this.projects = data);
  }

  removeProject(id: number): any {
    this.httpService.deleteProject(id).subscribe((data: IProject[]) => this.projects = this.projects.filter(x => x.id !== id));
  }
}


