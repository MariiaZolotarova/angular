import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { Observable } from 'rxjs';
import { ProjectDto } from './project-create/project-create.component';

@Injectable()
export class ProjectHttpService{

    constructor(private http: HttpClient){ }

    getProject(): Observable<object>{
        return this.http.get('https://localhost:44379/api/Projects');
    }

    deleteProject(id): Observable<object>{
        return this.http.delete(`https://localhost:44379/api/Projects/${id}`);
    }

    postProject(project: ProjectDto): Observable<object>{
        return this.http.post('https://localhost:44379/api/Projects', project);
    }

    getProjectById(id): Observable<object>{
        return this.http.get(`https://localhost:44379/api/Projects/${id}`);
    }

    updateProject(id, project: ProjectDto): Observable<object>{
        return this.http.put(`https://localhost:44379/api/Projects/${id}`, project);
    }
}
