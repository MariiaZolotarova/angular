import { Component, OnInit, Input  } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { UserHttpService } from 'src/app/user/user-service';
import { Router, ActivatedRoute } from '@angular/router';
import { ProjectHttpService } from '../project-service';

export interface ProjectDto {
  name: string;
  description: string;
  deadLine: string;
  authorId: string;
  teamId: number;
}

@Component({
  selector: 'app-project-create',
  templateUrl: './project-create.component.html',
  styleUrls: ['./project-create.component.css'],
  providers: [ProjectHttpService]
})

export class ProjectCreateComponent implements OnInit {

  @Input() project: ProjectDto = {} as ProjectDto;
  projectForm: FormGroup;
  projectId: string;
  checkoutForm;

  constructor(private httpService: ProjectHttpService, private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.projectId = this.route.snapshot.paramMap.get('id');

    if (this.projectId === null) {
      this.initProjectForm();
    }
    else {
      this.httpService.getProjectById(this.projectId).subscribe((data: ProjectDto) => {
        this.project = data;
        this.initProjectForm();
      });
    }
  }
  initProjectForm(): void {
    this.projectForm = new FormGroup({
      name: new FormControl(this.project.name, [
        Validators.required
      ]),
      description: new FormControl(this.project.description, [
        Validators.required
      ]),
      deadLine: new FormControl(this.project.deadLine, [
        Validators.required
      ]),
      authorId: new FormControl(this.project.authorId, [
        Validators.required
      ]),
      teamId: new FormControl(this.project.teamId, [
        Validators.required
      ])
    });
  }

  saveProject(): void {
    const newProject = this.projectForm.value;

    if (this.projectId === null) {
      this.httpService.postProject(newProject).subscribe(() => { });
      this.projectForm.reset();
    }
    else {
      this.httpService.updateProject(this.projectId, newProject).subscribe(() => { });
    }
  }
}
