import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { UserTableComponent } from './user/user-table/user-table.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatIconModule} from '@angular/material/icon';
import {MatButtonModule} from '@angular/material/button';
import { ToolbarComponent } from './toolbar/toolbar.component';
import {MatSidenavModule} from '@angular/material/sidenav';
import {CdkTableModule} from '@angular/cdk/table';
import {MatTableModule} from '@angular/material/table';
import { ProjectTableComponent } from './project/project-table/project-table.component';
import { TeamTableComponent } from './team/team-table/team-table.component';
import { TasksTableComponent } from './tasks/tasks-table/tasks-table.component';
import { TasksCreateComponent } from './tasks/tasks-create/tasks-create.component';
import { TeamCreateComponent } from './team/team-create/team-create.component';
import { ProjectCreateComponent } from './project/project-create/project-create.component';
import { UserCreateComponent } from './user/user-create/user-create.component';
import {MatInputModule} from '@angular/material/input';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatNativeDateModule, MatRippleModule} from '@angular/material/core';
import {MatSelectModule} from '@angular/material/select';
import { registerLocaleData } from '@angular/common';
import { LOCALE_ID, NgModule } from '@angular/core';
import localeUa from '@angular/common/locales/uk';
import { HttpClientModule } from '@angular/common/http';

registerLocaleData(localeUa, 'uk-ua');

@NgModule({
  declarations: [
    AppComponent,
    UserTableComponent,
    ToolbarComponent,
    ProjectTableComponent,
    TeamTableComponent,
    TasksTableComponent,
    TasksCreateComponent,
    TeamCreateComponent,
    ProjectCreateComponent,
    UserCreateComponent,

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatToolbarModule,
    MatIconModule,
    MatButtonModule,
    MatSidenavModule,
    CdkTableModule,
    MatTableModule,
    MatInputModule,
    FormsModule,
    ReactiveFormsModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatRippleModule,
    MatSelectModule,
    HttpClientModule
  ],
  providers: [
    { provide: LOCALE_ID, useValue: 'uk-ua' }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
