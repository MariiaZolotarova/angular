import { Component, OnInit, Input } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { UserHttpService } from '../user-service';
import { Router, ActivatedRoute } from '@angular/router';

export interface UserDto {
  firstName: string;
  lastName: string;
  email: string;
  birthday: string;
  teamId: number;
}

@Component({
  selector: 'app-user-create',
  templateUrl: './user-create.component.html',
  styleUrls: ['./user-create.component.css'],
  providers: [UserHttpService]
})
export class UserCreateComponent implements OnInit {

  @Input() user: UserDto = {} as UserDto;
  userForm: FormGroup;
  userId: string;

  checkoutForm;

  constructor(private httpService: UserHttpService, private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.userId = this.route.snapshot.paramMap.get('id');

    if (this.userId === null) {
      this.initUserForm();
    }
    else {
      this.httpService.getUserById(this.userId).subscribe((data: UserDto) => {
        this.user = data;
        this.initUserForm();
      });
    }
  }

  initUserForm(): void {
    this.userForm = new FormGroup({
      firstName: new FormControl(this.user.firstName, [
        Validators.required
      ]),
      lastName: new FormControl(this.user.lastName, [
        Validators.required
      ]),
      email: new FormControl(this.user.email, [
        Validators.required
      ]),
      birthday: new FormControl(this.user.birthday, [
        Validators.required
      ]),
      teamId: new FormControl(this.user.teamId, [
        Validators.required
      ])
    });
  }

  saveUser(): void {
    const newUser = this.userForm.value;

    if (this.userId === null) {
      this.httpService.postUser(newUser).subscribe(() => { });
      this.userForm.reset();
    }
    else {
      this.httpService.updateUser(this.userId, newUser).subscribe(() => { });
    }
  }
}
