import { UserDto } from '../user-create/user-create.component';
import { Component, OnInit } from '@angular/core';
import { UserHttpService } from '../user-service';

export interface IUser {
  id: number;
  firstName: string;
  lastName: string;
  email: string;
  phone: string;
  birthday: string;
  registredAt: string;
  teamId: number;
}


@Component({
  selector: 'app-user-table',
  templateUrl: './user-table.component.html',
  styleUrls: ['./user-table.component.css'],
  providers: [UserHttpService]
})

export class UserTableComponent implements OnInit {
  displayedColumns: string[] = ['id', 'firstName', 'lastName', 'email', 'phone', 'birthday', 'teamId', 'editUser'];

  users: IUser[];
  constructor(private httpService: UserHttpService) { }
  ngOnInit(): void {
    this.httpService.getUser().subscribe((data: IUser[]) => this.users = data);
  }

  removeUser(id: number): any {
    this.httpService.deleteUser(id).subscribe((data: IUser[]) => this.users = this.users.filter(x => x.id !== id));
  }
}
