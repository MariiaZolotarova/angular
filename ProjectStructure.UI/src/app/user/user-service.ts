import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { Observable } from 'rxjs';
import { UserDto } from './user-create/user-create.component';

@Injectable()
export class UserHttpService{

    constructor(private http: HttpClient){ }

    getUser(): Observable<object>{
        return this.http.get('https://localhost:44379/api/Users');
    }

    deleteUser(id): Observable<object>{
        return this.http.delete(`https://localhost:44379/api/Users/${id}`);
    }

    postUser(user: UserDto): Observable<object>{
        return this.http.post('https://localhost:44379/api/Users', user);
    }

    getUserById(id): Observable<object>{
        return this.http.get(`https://localhost:44379/api/Users/${id}`);
    }

    updateUser(id, user: UserDto): Observable<object>{
        return this.http.put(`https://localhost:44379/api/Users/${id}`, user);
    }
}
