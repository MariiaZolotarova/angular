import { UserTableComponent } from './user/user-table/user-table.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TeamTableComponent } from './team/team-table/team-table.component';
import { ProjectTableComponent } from './project/project-table/project-table.component';
import { TasksTableComponent } from './tasks/tasks-table/tasks-table.component';
import { UserCreateComponent } from './user/user-create/user-create.component';
import { TeamCreateComponent } from './team/team-create/team-create.component';
import { ProjectCreateComponent } from './project/project-create/project-create.component';
import { TasksCreateComponent } from './tasks/tasks-create/tasks-create.component';

const routes: Routes = [
  {path: 'user/user-table', component: UserTableComponent},
  {path: 'team/team-table', component: TeamTableComponent},
  {path: 'project/project-table', component: ProjectTableComponent},
  {path: 'tasks/tasks-table', component: TasksTableComponent},
  {path: 'user/user-create/:id', component: UserCreateComponent},
  {path: 'team/team-create', component: TeamCreateComponent},
  {path: 'team/team-create/:id', component: TeamCreateComponent},
  {path: 'project/project-create', component: ProjectCreateComponent},
  {path: 'project/project-create/:id', component: ProjectCreateComponent},
  {path: 'tasks/tasks-create', component: TasksCreateComponent},
  {path: 'tasks/tasks-create/:id', component: TasksCreateComponent},
  {path: 'user/user-create', component: UserCreateComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
