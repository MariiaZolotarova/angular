﻿using System;
using System.Collections.Generic;
using System.Linq;
using ProjectStructure.BL.Context.Entity;
using ProjectStructure.Services.Implementation;
using ProjectStructure.UnitTests.Helpers;
using Xunit;

namespace ProjectStructure.UnitTests
{
    public class CollectionServiceTests
    {
        [Fact]
        public async System.Threading.Tasks.Task GetSomeUserTasks_WhenGetTasksInProject_GetCountOfTasksSomeUserAsync()
        {
            // Arrange
            var context = DbContextHelper.GetProjectContext("8");

            #region mockedContextData

            var team = new Team
            {
                Name = "Team1"
            };
            var users = new List<User>
            {
                new User
                {
                    FirstName = "Foby",
                    TeamId = 1
                },
                new User
                {
                    TeamId = 1,
                    FirstName = "Robby"
                }
            };
            var projects = new List<Project>
            {
                new Project
                {
                    TeamId = 1,
                    AuthorId = 1,
                    Name = "Project1"
                },
                new Project()
                {

                    TeamId = 1,
                    AuthorId = 2,
                    Name = "Project2"
                }
            };
            var tasks = new List<Task>
            {
                new Task
                {
                    Name = "task1",
                    ProjectId = 1,
                    PerformerId = 1,
                },
                new Task
                {
                    Name = "task2",
                    ProjectId = 1,
                    PerformerId = 1
                },
                new Task
                {
                    Name = "task3",
                    ProjectId = 2,
                    PerformerId = 2
                }
            };
            context.Users.AddRange(users);
            context.Projects.AddRange(projects);
            context.Teams.Add(team);
            context.Tasks.AddRange(tasks);
            context.SaveChanges();

            #endregion

            var unitOfWork = new BL.UnitOfWork.UnitOfWork(context);
            var collectionService = new CollectionService(unitOfWork);

            // Act
            var result = await collectionService.ShowCountProjectsWithTasksAsync(1);

            //Assert
            Assert.Single(result);
            Assert.Equal(2, result.First().Value);
        }

        [Fact]
        public async System.Threading.Tasks.Task GetSomeUserTasks_WhenGetTasksForSomeUserWithSomeLength_GetFilteredTasksSomeUserAsync()
        {
            // Arrange
            var context =
                DbContextHelper.GetProjectContext(
                    "GetSomeUserTasks_WhenGetTasksForSomeUserWithSomeLength_GetFilteredTasksSomeUser");

            #region mockedContextData

            var team = new Team
            {
                Name = "Team1"
            };
            var users = new List<User>
            {
                new User
                {
                    FirstName = "Foby",
                    TeamId = 1
                },
                new User
                {
                    TeamId = 1,
                    FirstName = "Robby"
                }
            };
            var projects = new List<Project>
            {
                new Project
                {
                    TeamId = 1,
                    AuthorId = 1,
                    Name = "Project1"
                },
                new Project()
                {

                    TeamId = 1,
                    AuthorId = 2,
                    Name = "Project2"
                }
            };
            var tasks = new List<Task>
            {
                new Task
                {
                    Name = "task1",
                    ProjectId = 1,
                    PerformerId = 1,
                },
                new Task
                {
                    Name = "task2task3task3task3task3task3task3task3task3task3task3task3task3",
                    ProjectId = 1,
                    PerformerId = 1
                },
                new Task
                {
                    Name = "task3task3task3task3task3task3task3task3task3task3task3task3",
                    ProjectId = 2,
                    PerformerId = 2
                }
            };
            context.Users.AddRange(users);
            context.Tasks.AddRange(tasks);
            context.Projects.AddRange(projects);
            context.Teams.Add(team);
            context.SaveChanges();

            #endregion

            var unitOfWork = new BL.UnitOfWork.UnitOfWork(context);
            var collectionService = new CollectionService(unitOfWork);
            var userId = 1;

            // Act
            var result = await collectionService.ShowDesignedTasksForSpecialUserAsync(userId);

            //Assert
            Assert.Single(result);
            Assert.Equal(tasks[0].Name, result.First().Name);
        }

        [Fact]
        public async System.Threading.Tasks.Task GetSomeTasks_WhenGetTasksFinishedAtLastYear_GetFilteredTasksAsync()
        {
            // Arrange
            var context =
                DbContextHelper.GetProjectContext(
                    "GetSomeTasks_WhenGetTasksFinishedAtLastYear_GetFilteredTasks");

            #region mockedContextData

            var team = new Team
            {
                Name = "Team1"
            };
            var users = new List<User>
            {
                new User
                {
                    FirstName = "Foby",
                    TeamId = 1
                },
                new User
                {
                    TeamId = 1,
                    FirstName = "Robby"
                }
            };
            var projects = new List<Project>
            {
                new Project
                {
                    TeamId = 1,
                    AuthorId = 1,
                    Name = "Project1"
                },
                new Project()
                {

                    TeamId = 1,
                    AuthorId = 2,
                    Name = "Project2"
                }
            };
            var tasks = new List<Task>
            {
                new Task
                {
                    Name = "task1",
                    ProjectId = 1,
                    PerformerId = 1,
                    FinishedAt = DateTime.UtcNow.AddYears(-2),
                    State = StateEnum.Finished
                },
                new Task
                {
                    Name = "task2",
                    ProjectId = 1,
                    PerformerId = 1,
                    FinishedAt = DateTime.UtcNow,
                    State = StateEnum.Finished
                },
                new Task
                {
                    Name = "task3",
                    ProjectId = 2,
                    PerformerId = 2,
                    FinishedAt = DateTime.UtcNow,
                    State = StateEnum.Finished
                },
                new Task
                {
                    Name = "task4",
                    ProjectId = 1,
                    PerformerId = 1,
                    FinishedAt = DateTime.UtcNow,
                    State = StateEnum.Canceled
                }
            };
            context.Users.AddRange(users);
            context.Tasks.AddRange(tasks);
            context.Projects.AddRange(projects);
            context.Teams.Add(team);
            context.SaveChanges();

            #endregion

            var unitOfWork = new BL.UnitOfWork.UnitOfWork(context);
            var collectionService = new CollectionService(unitOfWork);
            var userId = 1;

            // Act
            var result = await collectionService.ShowPerformedTasksInCurrentYearAsync(userId);

            //Assert
            Assert.Single(result);
            Assert.Equal(tasks[1].Name, result.First().Name);
        }

        [Fact]
        public async System.Threading.Tasks.Task GetSomeTeams_WhenShowListOfTeamsOlderThan_GetFilteredTeamsAsync()
        {
            // Arrange
            var context =
                DbContextHelper.GetProjectContext(
                    "GetSomeTeams_WhenShowListOfTeamsOlderThan_GetFilteredTeams");

            #region mockedContextData

            var teams = new List<Team>
            {
                new Team
                {
                    Name = "Team1"
                },
                new Team
                {
                    Name = "Team2"
                }
            };

            var users = new List<User>
            {
                new User
                {
                    FirstName = "Foby",
                    TeamId = 1,
                    Birthday = DateTime.UtcNow.AddYears(-11)
                },
                new User
                {
                    TeamId = 1,
                    FirstName = "Robby",
                    Birthday = DateTime.UtcNow.AddYears(-11)
                },
                new User
                {
                    TeamId = 2,
                    FirstName = "Robby",
                    Birthday = DateTime.UtcNow.AddYears(-3)
                }
            };
            var projects = new List<Project>
            {
                new Project
                {
                    TeamId = 1,
                    AuthorId = 1,
                    Name = "Project1"
                },
                new Project()
                {

                    TeamId = 2,
                    AuthorId = 3,
                    Name = "Project2"
                }
            };
            var tasks = new List<Task>
            {
                new Task
                {
                    Name = "task1",
                    ProjectId = 1,
                    PerformerId = 1
                },
                new Task
                {
                    Name = "task2",
                    ProjectId = 1,
                    PerformerId = 1
                },
                new Task
                {
                    Name = "task3",
                    ProjectId = 2,
                    PerformerId = 2
                }
            };
            context.Users.AddRange(users);
            context.Tasks.AddRange(tasks);
            context.Projects.AddRange(projects);
            context.Teams.AddRange(teams);
            context.SaveChanges();

            #endregion

            var unitOfWork = new BL.UnitOfWork.UnitOfWork(context);
            var collectionService = new CollectionService(unitOfWork);

            // Act
            var result = await collectionService.ShowListOfTeamsOlderThanAsync();

            //Assert
            Assert.Equal(2, result.Count);
            Assert.Equal(2, result[0].Users.Count);
            Assert.Empty(result[1].Users);
        }

        [Fact]
        public async System.Threading.Tasks.Task GetSomeUserWithTasks_WhenShowListOfUsersWithSortedTasks_GetFilteredUsersWithTasksAsync()
        {
            // Arrange
            var context =
                DbContextHelper.GetProjectContext(
                    "GetSomeUserWithTasks_WhenShowListOfUsersWithSortedTasks_GetFilteredUsersWithTasks");

            #region mockedContextData

            var teams = new List<Team>
            {
                new Team
                {
                    Name = "Team1"
                },
                new Team
                {
                    Name = "Team2"
                },
                new Team
                {
                    Name = "Team3"
                }
            };

            var users = new List<User>
            {
                new User
                {
                    FirstName = "zzzzzzzzz",
                    TeamId = 1
                },
                new User
                {
                    TeamId = 1,
                    FirstName = "aaaaaaaaaaaaa"
                },
                new User
                {
                    TeamId = 2,
                    FirstName = "sssssssss"
                }
            };
            var projects = new List<Project>
            {
                new Project
                {
                    TeamId = 1,
                    AuthorId = 1,
                    Name = "Project1"
                },
                new Project()
                {

                    TeamId = 2,
                    AuthorId = 3,
                    Name = "Project2"
                },
                new Project()
                {

                    TeamId = 3,
                    AuthorId = 3,
                    Name = "Project3"
                }
            };
            var tasks = new List<Task>
            {
                new Task
                {
                    Name = "task1",
                    ProjectId = 1,
                    PerformerId = 1
                },
                new Task
                {
                    Name = "task2task2task2task2",
                    ProjectId = 1,
                    PerformerId = 1
                },
                new Task
                {
                    Name = "task3task3task3",
                    ProjectId = 2,
                    PerformerId = 2
                },
                new Task
                {
                    Name = "task4task4",
                    ProjectId = 2,
                    PerformerId = 2
                },
                new Task
                {
                    Name = "task5task5task5task5task5task5task5",
                    ProjectId = 3,
                    PerformerId = 3
                },
                new Task
                {
                    Name = "ta",
                    ProjectId = 3,
                    PerformerId = 3
                }
            };
            context.Users.AddRange(users);
            context.Tasks.AddRange(tasks);
            context.Projects.AddRange(projects);
            context.Teams.AddRange(teams);
            context.SaveChanges();

            #endregion

            var unitOfWork = new BL.UnitOfWork.UnitOfWork(context);
            var collectionService = new CollectionService(unitOfWork);

            // Act
            var result =await collectionService.ShowListOfUsersWithSortedTasksAsync();

            //Assert
            Assert.Equal(3, result.Count);
            Assert.Equal(users[1].FirstName, result[0].User.FirstName);
            Assert.Equal(users[2].FirstName, result[1].User.FirstName);
            Assert.Equal(users[0].FirstName, result[2].User.FirstName);
            Assert.Equal(tasks[2].Name, result[0].Tasks[0].Name);
            Assert.Equal(tasks[3].Name, result[0].Tasks[1].Name);
            Assert.Equal(tasks[4].Name, result[1].Tasks[0].Name);
            Assert.Equal(tasks[5].Name, result[1].Tasks[1].Name);
            Assert.Equal(tasks[1].Name, result[2].Tasks[0].Name);
            Assert.Equal(tasks[0].Name, result[2].Tasks[1].Name);
        }

        [Fact]
        public async System.Threading.Tasks.Task GetSomeStructureOfUser_ShouldReturnCorrectValueAsync()
        {
            // Arrange
            var context =
                DbContextHelper.GetProjectContext(
                    "GetSomeStructure_ShouldReturnCorrectValue");

            #region mockedContextData

            var teams = new List<Team>
            {
                new Team
                {
                    Name = "Team1"
                },
                new Team
                {
                    Name = "Team2"
                },
                new Team
                {
                    Name = "Team3"
                }
            };

            var users = new List<User>
            {
                new User
                {
                    FirstName = "zzzzzzzzz",
                    TeamId = 1
                },
                new User
                {
                    TeamId = 1,
                    FirstName = "aaaaaaaaaaaaa"
                },
                new User
                {
                    TeamId = 2,
                    FirstName = "sssssssss"
                }
            };
            var projects = new List<Project>
            {
                new Project
                {
                    TeamId = 1,
                    AuthorId = 1,
                    Name = "Project1",
                    CreatedAt = DateTime.UtcNow
                },
                new Project()
                {

                    TeamId = 1,
                    AuthorId = 1,
                    Name = "Project2",
                    CreatedAt = DateTime.UtcNow.AddYears(-2)
                },
                new Project()
                {

                    TeamId = 3,
                    AuthorId = 3,
                    Name = "Project3",
                    CreatedAt = DateTime.UtcNow.AddYears(-1)
                }
            };
            var tasks = new List<Task>
            {
                new Task
                {
                    Name = "task1",
                    ProjectId = 1,
                    PerformerId = 1,
                    State = StateEnum.Canceled
                },
                new Task
                {
                    Name = "task2task2task2task2",
                    ProjectId = 1,
                    PerformerId = 1,
                    State = StateEnum.Finished,
                    CreatedAt = DateTime.UtcNow.AddYears(-1),
                    FinishedAt = DateTime.UtcNow
                },
                new Task
                {
                    Name = "task3task3task3",
                    ProjectId = 2,
                    PerformerId = 2,
                    State = StateEnum.Finished,
                    CreatedAt = DateTime.UtcNow.AddYears(-3),
                    FinishedAt = DateTime.UtcNow
                },
                new Task
                {
                    Name = "task5task5task5",
                    ProjectId = 3,
                    PerformerId = 3,
                    State = StateEnum.Canceled
                }
            };
            context.Users.AddRange(users);
            context.Tasks.AddRange(tasks);
            context.Projects.AddRange(projects);
            context.Teams.AddRange(teams);
            context.SaveChanges();

            #endregion

            var unitOfWork = new BL.UnitOfWork.UnitOfWork(context);
            var collectionService = new CollectionService(unitOfWork);
            int userId = 1;
            // Act
            var result = await collectionService.StructOfUserAsync(userId);

            //Assert
            Assert.Equal(1, result.User.Id);
            Assert.Equal(projects[0].Id, result.Project.Id);
            Assert.Equal(2, result.TasksCount);
            Assert.Equal(1, result.NotFinishedTasks);
            Assert.Equal(tasks[1].Name, result.LongestTask.Name);
        }

        [Fact]
        public async System.Threading.Tasks.Task GetSomeStructureOfProject_ShouldReturnCorrectValueAsync()
        {
            // Arrange
            var context =
                DbContextHelper.GetProjectContext(
                    "GetSomeStructureOfProject_ShouldReturnCorrectValue");

            #region mockedContextData

            var teams = new List<Team>
            {
                new Team
                {
                    Name = "Team1"
                },
                new Team
                {
                    Name = "Team2"
                },
                new Team
                {
                    Name = "Team3"
                }
            };

            var users = new List<User>
            {
                new User
                {
                    FirstName = "zzzzzzzzz",
                    TeamId = 1
                },
                new User
                {
                    TeamId = 1,
                    FirstName = "aaaaaaaaaaaaa"
                },
                new User
                {
                    TeamId = 2,
                    FirstName = "sssssssss"
                }
            };
            var projects = new List<Project>
            {
                new Project
                {
                    TeamId = 1,
                    AuthorId = 1,
                    Name = "Project1",
                    Description = "Project1",
                    CreatedAt = DateTime.UtcNow
                },
                new Project
                {

                    TeamId = 1,
                    AuthorId = 1,
                    Name = "Project2",
                    Description = "Project2",
                    CreatedAt = DateTime.UtcNow.AddYears(-2)
                },
                new Project
                {

                    TeamId = 3,
                    AuthorId = 3,
                    Name = "Project3",
                    Description = "Project3",
                    CreatedAt = DateTime.UtcNow.AddYears(-1)
                }
            };
            var tasks = new List<Task>
            {
                new Task
                {
                    Name = "task1",
                    Description = "task1",
                    ProjectId = 1,
                    PerformerId = 1,
                    State = StateEnum.Canceled
                },
                new Task
                {
                    Name = "task2task2task2task2",
                    Description = "task2task2task2task2",
                    ProjectId = 1,
                    PerformerId = 1,
                    State = StateEnum.Finished,
                    CreatedAt = DateTime.UtcNow.AddYears(-1),
                    FinishedAt = DateTime.UtcNow
                },
                new Task
                {
                    Name = "task3task3task3",
                    Description = "task3task3task3",
                    ProjectId = 2,
                    PerformerId = 2,
                    State = StateEnum.Finished,
                    CreatedAt = DateTime.UtcNow.AddYears(-3),
                    FinishedAt = DateTime.UtcNow
                },
                new Task
                {
                    Name = "task5task5task5",
                    Description = "task5task5task5",
                    ProjectId = 3,
                    PerformerId = 3,
                    State = StateEnum.Canceled
                }
            };
            context.Users.AddRange(users);
            context.Tasks.AddRange(tasks);
            context.Projects.AddRange(projects);
            context.Teams.AddRange(teams);
            context.SaveChanges();

            #endregion

            var unitOfWork = new BL.UnitOfWork.UnitOfWork(context);
            var collectionService = new CollectionService(unitOfWork);
            // Act
            var result = await collectionService.StructOfProjectsAsync();

            //Assert
            Assert.Equal(projects[0].Id, result[0].Project.Id);
            Assert.Equal(tasks[1].Name, result[0].LongestTask.Name);
            Assert.Equal(tasks[0].Id, result[0].ShortestTask.Id);
            
            Assert.Equal(projects[1].Id, result[1].Project.Id);
            Assert.Equal(tasks[2].Name, result[1].LongestTask.Name);
            Assert.Equal(tasks[2].Name, result[1].ShortestTask.Name);
            Assert.Equal(projects.Count, result.Count);
        }
    }
}
